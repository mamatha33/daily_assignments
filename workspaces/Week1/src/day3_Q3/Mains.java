package day3_Q3;

public class Mains {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ThreeDimensionShape shape1 = new ThreeDimensionShape(5, 6, 7); 

	     ThreeDimensionShape shape2 = new ThreeDimensionShape(); 

	     ThreeDimensionShape shape3 = new ThreeDimensionShape(8); 
	     double volume;
	     volume=shape1.volume();
	        System.out.println(" Volume of shape1 is " + volume); 

	        volume = shape2.volume(); 

	        System.out.println(" Volume of shape2 is " + volume); 

	        volume = shape3.volume(); 

	        System.out.println(" Volume of shape3 is " + volume); 

	}

}
