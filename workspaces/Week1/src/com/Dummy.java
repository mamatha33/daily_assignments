package com;
import java .util.*;
public class Dummy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		//printing Name
		int i;
		String s;
		Scanner name = new Scanner (System.in);
		System.out.print("Enter the name : ");
		s = name.nextLine();
		System.out.println("Name: "+s);
		System.out.println(" ");

		//printing 5 tasks
		Scanner ss = new Scanner(System.in);
		String[] task = new String[5];
		System.out.println("Enter the five tasks: ");
		for (i=0; i<5; i++)
		{
		task[i] = ss.nextLine();
		 }
		System.out.println(" ");
		//task[0] = "wake up";
		//task[1] = "Bath";
		//task[2] = "Bath";
		//task[3] = "Break Fast";
		//task[4] = "Attend the morning session";
		int a= task.length;

		System.out.println("Five tasks are listed below: "+"\n");
		for (i=0; i<a; i++)
		{
		 System.out.println("Task "+i+": "+task[i]);

		}
		System.out.println(" ");

		while(true) {
		System.out.println("\n"+"Select any one option from below:: "+"\n"+"\n"+"1. Print the tasks in order."+"\n"+"2. Print the repeated task"+"\n"+"3. Update the task"+"\n"+"4. Delete the task"+"\n"+"5. Search the task");
		Scanner sc= new Scanner(System.in);
		System.out.print("\n"+"Enter your Choice: ");
		int c= sc.nextInt();

		do {

		if(c==0) {
		System.out.println("Select the correct option. ");
		}
		switch(c) {
		case 1:
		//tasks to be shown in order
		String temp;
		for(i=0; i<a-1; i++)
		{
		for(int j=1; j<a; j++)
		{
		if(task[j-1].compareTo(task[j])>0)
		  {
		   temp= task[j-1];
		   task[j-1] = task[j];
		   task[j] = temp;
		  }
		}
		 
		}
		for(i=0;i<a;i++)
		{
		System.out.println(task[i]);

		}
		System.out.println(" ");
		break;

		case 2:
		//For repeated task
		a=task.length;
		int flag=0;
		for (i=0; i<(a -1); i++)
		{
		if(task[i].equals(task[i+1]))
		   {
		       System.out.println("Repeated Task: "+task[i]);
		       flag++;
		       
		   }

		}
		if(flag==0)
		{
		System.out.println("No repeated tasks.");
		}
		System.out.println(" ");
		break;

		case 3:
		// Updating the task in an array
		System.out.println("Updating the task: ");
		Scanner sss = new Scanner(System.in);
		System.out.print("Enter the index number where you have to update(0-4) : ");
		int num = sss.nextInt();

		Scanner newtask= new Scanner(System.in);
		System.out.println("Enter the New task : ");
		String updatedtask = newtask.nextLine();

		task[num]= updatedtask;
		System.out.println(" ");

		for(i=0;i<a;i++)
		{
		System.out.println(task[i]);

		}
		System.out.println(" ");
		break;

		case 4:

		//Deleting a task in a array
		System.out.println("Deleting the task: ");
		Scanner z = new Scanner(System.in);
		System.out.print("Enter the index number where you have to delete(0-4) : ");
		int n1 = z.nextInt();


		System.out.println(" ");

		for(i=0;i<a;i++)
		{
		if(i==n1)
		{
		System.out.println(" ");
		continue;
		}
		System.out.println(task[i]);

		}
		System.out.println(" ");
		break;

		case 5:
		//Searching a task from array
		System.out.println("Searching a task: ");
		Scanner searchtask= new Scanner(System.in);
		System.out.print("Enter the task : ");
		String tt = searchtask.nextLine();
		System.out.println("");

		for(i=0; i<task.length;i++) {
		if(tt.equals(task[i]))
		{
		System.out.println("Task found at index "+i);

		}
		else
		System.out.println("No task found");
		}
		break;
		}
		break;
		}while(c>0);
		}

		}}