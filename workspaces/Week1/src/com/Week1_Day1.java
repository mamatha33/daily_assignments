package com;
import java.util.*;
public class Week1_Day1 {

	public static void main(String[] args) {

	//hint 1=========printing Name
	int i;
	String name;
	Scanner s = new Scanner (System.in);
	System.out.print("Enter the name : ");
	name = s.nextLine();
	System.out.println("Name: "+name);
	System.out.println(" ");

	//hint 2=======printing 5 tasks
	Scanner ss = new Scanner(System.in);
	String[] task = new String[5];
	System.out.println("Enter the five tasks: ");
	for (i=0; i<5; i++)
	 {
	task[i] = ss.nextLine();
	  }
	System.out.println(" ");
	//task[0] = "open the book";
	//task[1] = "open the book";
	//task[2] = "write";
	//task[3] = "read";
	//task[4] = "close the book";
	int a= task.length;

	for (i=0; i<a; i++)
	{
	 System.out.println("Task "+i+": "+task[i]);

	}
	System.out.println(" ");


	//hint 3====tasks to be shown in order
	String temp;
	for(i=0; i<a-1; i++)
	 {
	for(int j=1; j<a; j++)
	{
	if(task[j-1].compareTo(task[j])>0)
	  {
	   temp= task[j-1];
	   task[j-1] = task[j];
	   task[j] = temp;
	  }
	}
	 
	 }
	for(i=0;i<a;i++)
	{
	System.out.println(task[i]);

	}
	System.out.println(" ");

	//For repeated task
	for (i=0; i<(task.length -1); i++)
	{
	if(task[i].equals(task[i+1]))
	{
	System.out.println("Repeated Task: "+task[i]);
	}
	}
	System.out.println(" ");

	 // Updating the task in an array
	System.out.println("Updating the task: ");
	Scanner sss = new Scanner(System.in);
	System.out.print("Enter the index number where you have to update(0-4) : ");
	int num = sss.nextInt();
	Scanner newtask= new Scanner(System.in);
	System.out.println("Enter the New task : ");
	String updatedtask = newtask.nextLine();
	task[num]= updatedtask;
	System.out.println(" ");

	for(i=0;i<a;i++)
	{
	System.out.println(task[i]);

	}
	System.out.println(" ");

	//Deleting a task in a array
	System.out.println("Deleting the task: ");
	Scanner z = new Scanner(System.in);
	System.out.print("Enter the index number where you have to delete(0-4) : ");
	int n1 = z.nextInt();


	System.out.println(" ");

	for(i=0;i<a;i++)
	{
	if(i==n1)
	{
	System.out.println(" ");
	continue;
	}
	System.out.println(task[i]);

	}
	System.out.println(" ");

	//Searching a task from array
	System.out.println("Searching a task: ");
	Scanner searchtask= new Scanner(System.in);
	System.out.print("Enter the task : ");
	String tt = searchtask.nextLine();
	System.out.println("");

	for(i=0; i<task.length;i++) {
	if(tt.equals(task[i]))
	{
	System.out.println("Task found at index "+i);

	}
	}


	}
	}
