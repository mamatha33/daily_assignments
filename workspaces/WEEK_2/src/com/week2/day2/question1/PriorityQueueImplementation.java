package com.week2.day2.question1;

import java.util.*;

public class PriorityQueueImplementation {
	public static void main(String[] args) {

		// create priorityQueue Object
		PriorityQueue<Integer> queue = new PriorityQueue<>();

		// perform addition of elements to priorityQueue
		queue.add(10);
		queue.add(20);
		queue.add(15);
		queue.add(18);

		// print the top element of the priority queue
		System.out.println("top of element is " + queue.peek());

		// check if the priority queue contains a particular element
		System.out.println("does the queue contains correct element '10:'?:" + queue.contains(10));

		// print the size of the queue
		System.out.println("size of the queue " + queue.size());

		// print the head of the queue
		System.out.println("head of the queue " + queue.peek());

		// delete an element of the queue
		queue.remove(20);

		// print the queue elements
		System.out.println("elements of the queue are after removing: " + queue);

		// remove all the elements of the queue at once
		System.out.println("All elements are removed using clear: ");
		queue.clear();

		// check if the queue is empty or not
		System.out.println("checking queue is empty or not: " + queue.isEmpty());

	}

}