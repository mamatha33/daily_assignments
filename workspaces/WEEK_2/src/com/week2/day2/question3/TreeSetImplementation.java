package com.week2.day2.question3;
import java.util.TreeSet;
import java.util.Iterator;

public class TreeSetImplementation {
	public static void main(String[] args) {

		// create a treeSet object
		TreeSet<Integer> tree = new TreeSet<Integer>();

		// add elements to treeSet object
		tree.add(20);
		tree.add(39);
		tree.add(45);
		tree.add(15);

		// print the treeSet
		System.out.println("elements of the treeset are: " + tree);

		// use iterator to print the elements
		Iterator itr = tree.iterator();
		System.out.println("the values of treeset");
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

		// print the first element
		System.out.println("the first element in the treeSet: " + tree.first());

		// print the last element
		System.out.println("the last element in the tree set: " + tree.last());

		// retrieve and remove the last element
		System.out.println("retrieve and removing the last element: " + tree.pollLast());

		// print the size of the treeSet
		System.out.print("the size of the treeset: " + tree.size());

	}

}