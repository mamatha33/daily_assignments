package com.week2.day2.question2;
import java.util.ArrayDeque;

import java.util.Deque;

public class DequeueImplementation {

	public static void main(String[] args) {

// Create deque object using ArrayDeque Class

		Deque<Integer> deque = new ArrayDeque<Integer>();

// add elements to the deque
		deque.add(67);
		deque.add(35);
		deque.add(45);
		deque.add(25);

// insert an element at the head
		deque.addFirst(90);
		System.out.println("after adding at the head deque elements are " + deque);

// insert an element at the tail
		deque.addLast(43);
		System.out.println("after adding element at the tail " + deque);

// print the deque
		System.out.println("total elements of the dequeue " + deque);

//peek the first element
		System.out.println("first element: " + deque.peek());

//delete all the elements of deque
		System.out.println("deleting all elements from the dqueue ");
		deque.clear();
		System.out.println("elements in the dqueue: " + deque);
		System.out.println("deque is empty or not: " + deque.isEmpty());

	}

}