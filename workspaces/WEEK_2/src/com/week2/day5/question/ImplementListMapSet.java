package com.week2.day5.question;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class ImplementListMapSet {
	public static void main(String args[]) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("girl");
		list.add("dancing");
		list.add("tv");
		list.add("fruit");
		System.out.println("arrayList elements are: ");
		for (String values : list) {
			System.out.println(values);
		}
		System.out.println(" ");

		System.out.println("HashMap values are");
		HashMap<String, Integer> elements = new HashMap<String, Integer>();
		elements.put("Amruta", 32);
		elements.put("Abhi", 34);
		elements.put("Aradhya", 36);
		for (String i : elements.keySet()) {
			System.out.println("key: " + i + ", value: " + elements.get(i));
		}
		System.out.println(" ");
		System.out.println("hashset values are");
		HashSet<String> set = new HashSet<String>();
		set.add("India");
		set.add("europe");
		set.add("kerala");
		set.add("america");
		for (String i : set) {
			System.out.println(i);
		}
	}

}