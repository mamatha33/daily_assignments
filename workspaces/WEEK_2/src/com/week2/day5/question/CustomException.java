package com.week2.day5.question;

import java.util.*;

public class CustomException extends Exception {
	public CustomException(String msg) {
		super(msg);
	}
}
