package com.week2.day5.question;

import java.util.*;

public class FruitChecker {
	ArrayList<String> fruits = new ArrayList<>(Arrays.asList("mango", "banana", "Guvva"));

	public void checkFruit(String fruit) throws CustomException {
		if (fruits.contains(fruit)) {
			throw new CustomException(fruit + " already exists");
		} else {
			fruits.add(fruit);
			System.out.println(fruit + " is added to arraylist");

		}
	}

	public static void main(String[] args) {

		// create ArrrayList of type String to store fruits
		ArrayList<String> fruits = new ArrayList<String>();

		// create object of FruitChecker class
		FruitChecker fc = new FruitChecker();

		// inside try catch block call checkFruit method to catch exception if any

		try {
			// fc.checkFruit("mango");
			fc.checkFruit("papaya");
			fc.checkFruit("mango");

		}

		catch (CustomException e) {

			System.out.println(e);

		}
	}
}