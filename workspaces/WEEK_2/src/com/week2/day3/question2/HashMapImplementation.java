package com.week2.day3.question2;
import java.util.*;
public class HashMapImplementation {
	public static void main(String[] args) {
		// create HashMap object
		HashMap<Integer, String> value = new HashMap<Integer, String>();

		// add hashMap values
		value.put(1, "Mamatha");
		value.put(2, "Sleeping");
		value.put(3, "dancing");
		value.put(1, "working");
		value.put(4, "cooking");

		// print the hashCode of the hashMap object
		System.out.println("hashcode of hashmap object: " + value.hashCode());

		// print the size of the map
		System.out.println("size of the map: " + value.size());

		// print the hashMap
		System.out.println("map elements are: " + value);

		// check if a particular key is present
		boolean key = value.containsKey(3);
		System.out.println("The 3 key exists in hashmap?: " + key);

		// print only the values of the hashMap
		System.out.println("values of the hashmap: " + value.values());

		// check if a particular value is present
		boolean values = value.containsValue("cooking");
		System.out.println("the value cooking is exist: " + values);

		// print only the key set of the hashMap
		System.out.println("only key set in the map: " + value.keySet());

		// remove a particular key
		value.remove(1);
		System.out.println(value);

		// iterate using forEach and Map.Entry
		for (Map.Entry<Integer, String> entry : value.entrySet())
			System.out.println("key=" + entry.getKey() + " value= " + entry.getValue());

	}
}