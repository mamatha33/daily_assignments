package com.week2.day4.question1;
import java.util.Scanner;

public class NestedTryCatch {
	public static void main(String args[]) {
		
			int n ;
			Scanner sc = new Scanner(System.in);
			System.out.println("enter the array size");
			n = sc.nextInt();
			int array[] = new int[n];
			System.out.println("enter array elements");
			for (int i = 0; i <n; i++) {
				array[i] = sc.nextInt();
			}
			for(int i=0;i<n;i++)
				System.out.println("array elements are: " + array[i]);
			System.out.println("random array element");
			int random=(int) Math.random();
			System.out.println(random);
			try {
				array[1]=100;
			try {
				int y = array[2] / 0;
			} 
			catch (ArithmeticException e) {
				System.out.println("divison by zero is not possible:Arithmatic array exception");
			}
			
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("array out of bond Exception");

		}
	}

}