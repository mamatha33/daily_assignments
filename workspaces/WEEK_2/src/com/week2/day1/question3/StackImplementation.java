package com.week2.day1.question3;
import java.util.Stack;

import java.util.*;

public class StackImplementation {
	public static void main(String args[])

	{

		// Create an object of Stack of type String

		Stack<String> stack = new Stack<String>();

		// add elements into the Stack
		stack.push("reading");
		stack.push("writing");
		stack.push("dancing");
		stack.push("cooking");

		System.out.println("the elements in the stack: " + stack);
		System.out.println("the top most element is: " + stack.peek());
		System.out.println("the elements in the stack: " + stack);
		System.out.println("the element is removed: " + stack.pop());
		System.out.println("the elements in the stack: " + stack);
		// Display the Stack

		// Create an iterator
		Iterator value = stack.iterator();
		System.out.println("the iterator valuse are: ");
		while (value.hasNext()) {
			System.out.println(value.next());

		}

	}
}