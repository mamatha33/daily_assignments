package com.week2.day1.arraylist;
	import java.util.ArrayList;
	import java.util.Scanner;

	public class ArrayListImplementation {
		public static void main(String[] args) {

			// Create and array List object
			ArrayList<Integer> element = new ArrayList<>();
			System.out.println("size of the arraylist: " + element.size());

			// add elements in the arrayList
			element.add(10);
			element.add(20);
			element.add(57);
			element.add(15);
			element.add(25);

			// print the size of the list
			System.out.println("size of arraylist after elements added: " + element.size());
			System.out.println();
			// print the minimum value present of the list
			int min = element.get(0);
			for (int i = 1; i < element.size(); i++) {
				if (min > element.get(i))
					min = element.get(i);
			}

			System.out.println("the minimum element in the arraylist is: " + min);

			// maximum element
			int max = element.get(0);
			for (int i = 1; i < element.size(); i++) {
				if (max < element.get(i))
					max = element.get(i);
			}
			System.out.println("maximum element in the array list: " + max);
			System.out.println();
			// arrayList is empty or not
			System.out.print("checking arrayList is empty or not: ");
			System.out.println(element.isEmpty());

			// remove element using index
			System.out.print("enter the index to remove the element: ");
			Scanner sc = new Scanner(System.in);
			int index = sc.nextInt();
			System.out.println("the element is removed from the list: " + element.remove(index));

			// remove by the value in the method

			int ele = element.remove(1);
			System.out.println("the element is removed using remove method: " + ele);
			System.out.println("size of array list after removing: " + element.size());
			// displaying
			System.out.println("the remiaing elements in arrayList after removing: ");
			for (int al : element) {
				System.out.println(al);
			}
		}

	}