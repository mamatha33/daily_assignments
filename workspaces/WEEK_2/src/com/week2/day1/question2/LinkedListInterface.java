package com.week2.day1.question2;
	import java.util.Collections;
	import java.util.LinkedList;
	import java.util.Scanner;

	public class LinkedListInterface {
		public static void main(String[] args) {

			// create a linked list
			LinkedList<Integer> element = new LinkedList<Integer>();

			// create a scanner object
			Scanner sc = new Scanner(System.in);

			System.out.println("enter the number elements you want to be add");
			int num = sc.nextInt();
			System.out.println("the elements are");
			for (int i = 0; i < num; i++) {

				System.out.print("add element:");
				int elements = sc.nextInt();
				element.add(elements);

			}
			Collections.sort(element);
			System.out.println("increasing order: " + element);
			Collections.sort(element, Collections.reverseOrder());
			System.out.println("decresing order: " + element);

		}

	}